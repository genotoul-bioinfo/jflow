\documentclass{bioinfo}
\copyrightyear{2014}
\pubyear{2014}

\begin{document}
\firstpage{1}

\title[short Title]{Jflow: a workflow management system for web applications}
\author[Mariette \textit{et~al}]{J\'{e}r\^{o}me Mariette\,$^{1,*}$,
Fr\'{e}d\'{e}ric Escudi\'{e}\,$^{1}$, Philippe Bardou\,$^2$, Ibouniyamine
Nabihoudine\,$^1$, C\'{e}line Noirot\,$^1$, Marie-St\'{e}phane Trotard\,$^1$,
Christine Gaspin\,$^{1}$ and Christophe Klopp\,$^{1,2}$}
\address{$^{1}$Plate-forme bio-informatique Genotoul, INRA, UR875 Mathématiques
et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France.\\ 
$^{2}$Plate-forme SIGENAE, INRA, GenPhyse, BP 52627, 31326 Castanet-Tolosan Cedex, France.}

\history{Received on XXXXX; revised on XXXXX; accepted on XXXXX}

\editor{Associate Editor: XXXXXXX}

\maketitle

\begin{abstract}

\section{Summary:}
Biologists produce large data sets and are in demand of rich and simple
web portals in which they can upload and analyze their files. Providing such
tools requires to mask the complexity induced by the needed High Performance
Computing (HPC) environment. The connexion between interface and computing
infrastructure is usually specific to each portal. With Jflow, we introduce
a Workflow Management System (WMS), composed of jQuery plug-ins which can easily
be embedded in any web application and a Python library providing all requested
features to setup, run and monitor workflows.

\section{Availability:}
Jflow is available under the GNU General Public License (GPL) at
http://bioinfo.genotoul.fr/jflow. The package is coming with full
documentation, quick start and a running test portal.

\section{Contact:}
\href{support.genopole@toulouse.inra.fr}{support.genopole@toulouse.inra.fr}
\end{abstract}

\section{Introduction}

Building rich web environments aimed at helping scientists analyze their data is
a common trend in bioinformatics. Specialized web portals such as MG-RAST (Meyer
et al., 2008), MetaVir (Roux et al., 2011) or NG6 (Mariette et al., 2012) provide
multiple services and analysis tools in an integrated manner for specific
experiments or data types. These applications require WMS features to manage
and execute their computational pipelines.

Generic WMS, such as Galaxy (Goecks et al., 2010), Ergatis (Orvis et al., 2010)
or Mobyle (Wilkinson et al., 2009) provide a user friendly graphical interface
easing workflow creation and execution. Unfortunately, such environments come
with their own interface, complicating their integration within already existing
web tools. Other WMS such as weaver (Bui et al., 2012), Snakemake (Koster et
al., 2012), Ruffus (Goodstadt, 2010) or Cosmos (Gafni et al., 2014) provide a
framework or a domain-specific language to developers wanting to build and run
workflows. These software packages offer the flexibility and power of a high
level programming language, but they do not provide a user interface, enable
component and workflow definition.

JFlow combines a user friendly interface with an intuitive python API. It
is, to our knowledge, the only WMS designed to be embedded in any web application,
thanks to its organization as jQuery (http://jquery.com/) plug-ins. 

\section{Methods}

Jflow user interface gathers five jQuery plug-ins providing user oriented views.

\begin{itemize}
  \item \textit{availablewf} lists all runnable workflows accessible to users,
  \item \textit{activewf} monitors all started, completed, failed, aborted and
  reseted workflows,
  \item \textit{wfform} presents workflow editable parameters in a form,
  \item \textit{wfoutputs} displays all outputs produced by the workflow
  organized per component,
  \item \textit{wfstatus} shows the workflow execution state as a list or an
  execution graph. The graph visualization uses the Cytoscape web JavaScript
  plug-in (Lopes et al., 2010).
\end{itemize}

The plug-ins give access to multiple communication methods and events. They
interact with the server side through Jflow's REST API, running under a cherrypy
(http://www.cherrypy.org/) web server. The included server uses the JSONP
communication technique enabling cross-domain requests. 

To be available from the different jQuery plug-ins, the workflows have to be
implemented using the Jflow API. A Jflow component is in charge of an execution
step. Adding a component to the system requires to write a Python
\textit{Component} subclass. In Jflow, different solutions are available to ease
component creation. To wrap a single command line, the developer can give a
position or a flag for each parameter. Jflow also embeds an XML parser which
allows to run genuine Mobyle (Wilkinson et al., 2009) components. Finally, to
allow developers to integrate components from other WMS, Jflow provides a 
skeleton class. This class only requires to implement the parsing step. A
workflow chains components. It is represented by a directed acyclic graph (DAG)
where nodes represent jobs and edges links between inputs and outputs. When
paths are disjoint, jobs are run in parallel. A Jflow workflow is built as a
\textit{Workflow} subclass. Components are added to the workflow as variables
and chained linking outputs and inputs.

To define the parameters presented to the final user, Jflow gives access to
different class methods. Each parameter has at least a name, a user help text
and a data type. For file or directory parameters, it is possible to set
required file format, size limitation and location. Jflow handles server side
files with regular expressions, but also URLs and client side files, in which
case, it automatically uploads them. Before running the workflow, Jflow checks
data type compliance for each parameter. Job submission, status
checking and error handling, rely on Makeflow (Albrecht et al., 2012) and
weaver (Bui et al., 2012). Therefore Jflow manages error recovery and supports
most distributed resource management systems (Condor, SGE, Work Queue or a
single multi core machine, \ldots). Replacing Makeflow by an other job submitter
requires to implement a new \textit{Engine} subclass. This class creates and
executes the workflow DAG.


\section{Example}

Jflow user interface has been designed to allow an easy integration in mash up
web applications. Hereunder, we present its integration in NG6, which provides a
user-friendly interface to process, store and download high-throughput
sequencing data. The environment displays sequencing runs as a table. From
this view, the user can add new data by running workflows in charge of loading
the data and checking its quality. Different workflows are available considering
data type and sequencing technology.

Workflows are listed by the \textit{availablewf} plug-in built within a
NG6 modal box. A \textit{select.availablewf} event thrown by the
\textit{availablewf} plug-in is listened and caught to generate the parameter
form using the \textit{wfform} plug-in. Considering the parameter type, Jflow
adapts its display. For example, a date is shown as a calendar and a boolean
as a check box.

Biologists use NG6 to check sequencing reads quality, including experimental
samples contamination measure. The first input of this analysis is the
contaminant reference genome fasta file, displayed as a file selector. The
second input is a parameter set describing the biological samples. It includes
the read files and meta data such as sample name, tissue and development stage.
To help biologists populate it, Jflow uses a structured data input rendered by
the \textit{wfform} plug-in as a spreadsheet. It allows to copy and paste
multiple lines. Jflow iterates then on the table content to launch each sample
processing in parallel.

\begin{figure}[ht]
	\centering
	\includegraphics[width=\linewidth]{jflow_example.png}
	\caption{\textbf{Jflow integration:} (a) A piece of the NG6 HTML code source in
	which is positioned an empty div to build the \textit{activewf} plug-in
and a
	modal box for the \textit{wfstatus} plug-in. (b) The jQuery code in
charge to
	build Jflow plug-ins and manage user action. When the
\textit{select.activewf}
	event is thrown from \textit{activewf-div}, a function is called with two
	parameters: \textit{event} and \textit{workflow}. The last parameter stores
	all the workflow's information, such as its name and its id, used in this
	example to update the modal box title and to build the \textit{wfstatus}
	plug-in. (c)  The status of the illumina\_qc workflow with the id 26 displayed
	as a graph in the NG6 application.}
	\label{fig::jflow_example}
\end{figure}


To monitor running workflows, NG6 provides a table in a specific page. The
table is filled by the \textit{activewf} plug-in. In the same way as
described above, the \textit{wfstatus} is built on a modal box when a
\textit{select.activewf} event is thrown by the \textit{activewf} plug-in, as
presented in Figure~\ref{fig::jflow_example}. This view shows the workflow's
execution graph where nodes represent components and edges links between inputs
and outputs.

NG6 was first implemented using the Ergatis (Orvis et al., 2010) WMS, which had
a separate user interface. With Jflow, all actions are now available from the
same application, which makes it user friendly.

\section{Conclusion}

Jflow is a simple and efficient solution to embed WMS features within a web
application. It is, to our knowledge, the only WMS designed with this purpose.
It is already embedded in RNAbrowse (Mariette et al., 2014) and NG6 (Mariette et
al., 2012), where it has been used to process more than 2 000 sequencing runs
on a 5 000 cores HPC environment.

\paragraph{Conflict of Interest\textcolon} none declared.

\begin{thebibliography}{}

\bibitem[Albrecht {\it et~al}., 2012]{Albrecht} Albrecht M, Donnelly P, Bui P
and Thain D. (2012) Makeflow: A Portable Abstraction for Data Intensive
Computing on Clusters, Clouds, and Grids. {\it SWEET at ACM SIGMOD}, {\bf20}.

\bibitem[Bui, 2012]{Bui} Bui P, (2012) Compiler Toolchain For Data Intensive
Scientific Workflows. {\it Ph.D. Thesis, University of Notre Dame}.

\bibitem[Gafni {\it et~al}., 2014]{Gafni} Gafni E, Luquette L, Lancaster A,
Hawkins J, Jung J-Y, Souilmi Y, Wall D and Tonellato P. (2014) COSMOS: Python
library for massively parallel workflows. {\it Bioinformatics}, {\bf30},
2956-2958.

\bibitem[Goecks {\it et~al}., 2010]{Goecks} Goecks J, Nekrutenko A, Taylor J and
The Galaxy Team. (2010) Galaxy: a comprehensive approach for supporting
accessible, reproducible, and transparent computational research in the life
sciences, {\it Genome Biology}, {\bf 11}, R86.

\bibitem[Goodstadt, 2002]{Goodstadt} Goodstadt L. (2010) Ruffus: a lightweight
Python library for computational pipelines. {\it Bioinformatics}, {\bf26},
2778-2779.

\bibitem[Koster {\it et~al}., 2002]{Koster} K\"oster J and Rahmann S. (2012)
Snakemake-a scalable bioinformatics workflow engine. {\it Bioinformatics},
{\bf28}, 2520-2522.

\bibitem[Lopes {\it et~al}., 2010]{Lopes} Lopes CT, Franz M, Kazi F, Donaldson
SL, Morris Q and Bader GD. (2010) Cytoscape Web: an interactive web-based
network browser, {\it Bioinformatics}, {\bf 26}, 2347-2348.

\bibitem[Mariette {\it et~al}., 2012]{Mariette} Mariette J, Escudie F, Allias
N, Salin G, Noirot C, Thomas S and Klopp C. (2012) NG6: Integrated next
generation sequencing storage and processing environment. {\it BMC Genomics},
13:462.

\bibitem[Mariette {\it et~al}., 2014]{Mariette} Mariette J, Noirot C,
Nabihoudine I, Bardou P, Hoede C, Djari A, Cabau C and Klopp C. (2014)
RNAbrowse: RNA-Seq De Novo Assembly Results Browser. {\it PLoS ONE}, 9(5).

\bibitem[Meyer {\it et~al}., 2008]{Meyer} Meyer F, Paarmann D, D'Souza M, Olson
R, Glass EM, Kubal M, Paczian T, Rodriguez A, Stevens R, Wilke A, Wilkening J
and Edwards RA. (2008) The metagenomics RAST server – a public resource for the
automatic phylogenetic and functional analysis of metagenomes, {\it BMC
Bioinformatics}, {\bf 9:386}.

\bibitem[Orvis {\it et~al}., 2010] Orvis J, Crabtree J, Galens K, Gussman A,
Inman JM, Lee E, Nampally S, Riley D, Sundaram JP, Felix V, Whitty B, Mahurkar
A, Wortman J, White O, Angiuoli SV. (2010) Ergatis: A web interface and scalable
software system for bioinformatics workflows. {\it Bioinformatics}, 15;26(12).

\bibitem[Roux {\it et~al}., 2011]{Roux} Roux S, Faubladier M, Mahul A, Paulhe N,
Bernard A, Debroas D, Enault F. (2011) Metavir: a web server dedicated to virome
analysis, {\it Bioinformatics}, {\bf 21}, 3074-3075.

\bibitem[Wilkinson {\it et~al}., 2002]{Wilkinson} Wilkinson MD and Links M. (2009)
BioMOBY: an open source biological web services proposal. {\it Bioinformatics},
{\bf25}, 3005-3011.

\end{thebibliography}
\end{document}
