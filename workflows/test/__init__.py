#
# Copyright (C) 2015 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import sys

from jflow.workflow import Workflow
from jflow.parameter import InputFileList, InputFile


class Test (Workflow):
    def get_description(self):
        return "use for test jflow core modifications"

    def define_parameters(self, parameters_section=None):
        self.add_parameter("test_E1", "float null.", type="float", group="test group")
        self.add_parameter("test_E2", "float default 1.", type="float", default=1, group="test group")
        self.add_parameter("test_E3", "int default 2.", type=int, default=2, group="test group")
        self.add_parameter("test_E4", "int null.", type=int, group="test group")
        self.add_parameter("test_E5", "date default str 20/08/2014.", type="date", default="20/08/2014", group="test group")
        self.add_parameter("test_E6", "date default null.", type="date", group="test group")
        self.add_parameter("test_E7", "string default str None.", default="None", group="test group")
        self.add_parameter("test_E8", "string null.", group="test group")
        self.add_parameter("test_E9", "bool default True.", type="bool", default=True, group="test group")
        self.add_parameter("test_E10", "bool default false.", type="bool", default=False, group="test group")
        self.add_parameter("test_E11", "bool default null.", type="bool", group="test group")
        self.add_input_file("test_E12", "InputFile default null.", group="test group")
        self.add_input_file("test_E13", "InputFile default application.properties.", default="application.properties", group="test group")
#         self.add_multiple_parameter_list("multi1", "Multiple.")
#         self.add_parameter("test_A1", "Multiple float null.", add_to="multi1", type="float")
#         self.add_parameter("test_A2", "Multiple float default 1.", add_to="multi1", type="float", default=1)
#         self.add_parameter("test_A3", "Multiple int default 2.", add_to="multi1", type=int, default=2)
#         self.add_parameter("test_A4", "Multiple int null.", add_to="multi1", type=int)
#         self.add_parameter("test_A5", "Multiple date default str '20/08/2014'.", add_to="multi1", type="date", default="20/08/2014")
#         self.add_parameter("test_A6", "date default null.", add_to="multi1", type="date")
#         self.add_parameter("test_A7", "Multiple string default str 'None'.", add_to="multi1", default="None")
#         self.add_parameter("test_A8", "Multiple string null.", add_to="multi1")
#         self.add_parameter("test_A9", "Multiple bool default True.", add_to="multi1", type="bool", default=True)
#         self.add_parameter("test_A10", "Multiple bool default 3.", add_to="multi1", type="bool", default=3)
#         self.add_parameter("test_A11", "Multiple bool default null.", add_to="multi1", type="bool")
#         self.add_input_file("test_A12", "Multiple InputFile default null.", add_to="multi1")
#         self.add_input_file("test_A13", "Multiple InputFile default application.properties.", add_to="multi1", default="application.properties")
        self.add_parameter("test_B1", "float null.", type="float")
        self.add_parameter("test_B2", "float default 1.", type="float", default=1)
        self.add_parameter("test_B3", "int default 2.", type=int, default=2)
        self.add_parameter("test_B4", "int null.", type=int)
        self.add_parameter("test_B5", "date default str 20/08/2014.", type="date", default="20/08/2014")
        self.add_parameter("test_B6", "date default null.", type="date")
        self.add_parameter("test_B7", "string default str None.", default="None")
        self.add_parameter("test_B8", "string null.")
        self.add_parameter("test_B9", "bool default True.", type="bool", default=True)
        self.add_parameter("test_B10", "bool default false.", type="bool", default=False)
        self.add_parameter("test_B11", "bool default null.", type="bool")
        self.add_input_file("test_B12", "InputFile default null.")
        self.add_input_file("test_B13", "InputFile default application.properties.", default="application.properties")
        self.add_input_file_list("test_B16", "InputFileList default null.")
#         self.add_multiple_parameter_list("multi2", "Multiple list required.", required=True)
#         self.add_parameter("valid", "Multiple required.", add_to="multi2", default="ok")
#         self.add_parameter("test_C1", "Multiple float null.", add_to="multi2", type="float")
#         self.add_parameter("test_C2", "Multiple float default 1.", add_to="multi2", type="float", default=1)
#         self.add_parameter("test_C3", "Multiple int default 2.", add_to="multi2", type=int, default=2)
#         self.add_parameter("test_C4", "Multiple int null.", add_to="multi2", type=int)
#         self.add_parameter("test_C5", "Multiple date default str '20/08/2014'.", add_to="multi2", type="date", default="20/08/2014")
#         self.add_parameter("test_C6", "date default null.", add_to="multi2", type="date")
#         self.add_parameter("test_C7", "Multiple string default str 'None'.", add_to="multi2", default="None")
#         self.add_parameter("test_C8", "Multiple string null.", add_to="multi2")
#         self.add_parameter("test_C9", "Multiple bool default True.", add_to="multi2", type="bool", default=True)
#         self.add_parameter("test_C10", "Multiple bool default 3.", add_to="multi2", type="bool", default=3)
#         self.add_parameter("test_C11", "Multiple bool default null.", add_to="multi2", type="bool")
#         self.add_input_file("test_C12", "Multiple InputFile default null.", add_to="multi2")
#         self.add_input_file("test_C13", "Multiple InputFile default application.properties.", add_to="multi2", default="application.properties")##### Pb localfile and urlfile
#         self.add_parameter_list("test_C14", "Multiple ParameterList [1, 2, 3]", add_to="multi2", default=[1, 2, 3])
#         self.add_parameter_list("test_C15", "Multiple ParameterList default null", add_to="multi2")
#         self.add_input_file_list("test_C16", "InputFileList default null.", add_to="multi2")
        self.add_multiple_parameter("multi3", "Multiple required.")
        self.add_parameter("valid", "Multiple required.", add_to="multi3", default="ok")
        self.add_parameter("test_D1", "Multiple float null.", add_to="multi3", type="float")
        self.add_parameter("test_D2", "Multiple float default 1.", add_to="multi3", type="float", default=1)
        self.add_parameter("test_D3", "Multiple int default 2.", add_to="multi3", type=int, default=2)
        self.add_parameter("test_D4", "Multiple int null.", add_to="multi3", type=int)
        self.add_parameter("test_D5", "Multiple date default str '20/08/2014'.", add_to="multi3", type="date", default="20/08/2014")
        self.add_parameter("test_D6", "date default null.", add_to="multi3", type="date")
        self.add_parameter("test_D7", "Multiple string default str 'None'.", add_to="multi3", default="None")
        self.add_parameter("test_D8", "Multiple string null.", add_to="multi3")
        self.add_parameter("test_D9", "Multiple bool default True.", add_to="multi3", type="bool", default=True)
        self.add_parameter("test_D10", "Multiple bool default 3.", add_to="multi3", type="bool", default=3)
        self.add_parameter("test_D11", "Multiple bool default null.", add_to="multi3", type="bool")
        self.add_input_file("test_D12", "Multiple InputFile default null.", add_to="multi3")
        self.add_input_file("test_D13", "Multiple InputFile default application.properties.", add_to="multi3", default="application.properties")##### Pb localfile and urlfile
        self.add_parameter_list("test_D14", "Multiple ParameterList [1, 2, 3]", add_to="multi3", default=[1, 2, 3])
        self.add_parameter_list("test_D15", "Multiple ParameterList default null", add_to="multi3")
        self.add_input_file_list("test_D16", "InputFileList default null.", add_to="multi3")


    def process(self):
        sys.stdout = open(os.path.join(self.directory, "wf_stdout"), 'w')
        print ("###################### Workflow ############################")
        for attr in self.params_order:
            value = getattr(self, attr)
            print (attr)
            print (value)
            print(("class   : " + value.__class__.__name__))
            print(("== None : " + str(value == None)))
            print ("")
        sys.stdout.close()
#         test_parameters = self.add_component("TestComponentParameters", [
#                                                                          self.test_B1,
#                                                                          self.test_B2,
#                                                                          self.test_B3,
#                                                                          self.test_B4,
#                                                                          self.test_B5,
#                                                                          self.test_B6,
#                                                                          self.test_B7,
#                                                                          self.test_B8,
#                                                                          self.test_B9,
#                                                                          self.test_B10,
#                                                                          self.test_B11,
#                                                                          self.test_B12,
#                                                                          self.test_B13,
#                                                                          self.multi2[0]["test_C1"],
#                                                                          self.multi2[0]["test_C2"],
#                                                                          self.multi2[0]["test_C3"],
#                                                                          self.multi2[0]["test_C4"],
#                                                                          self.multi2[0]["test_C5"],
#                                                                          self.multi2[0]["test_C6"],
#                                                                          self.multi2[0]["test_C7"],
#                                                                          self.multi2[0]["test_C8"],
#                                                                          self.multi2[0]["test_C9"],
#                                                                          self.multi2[0]["test_C10"],
#                                                                          self.multi2[0]["test_C11"],
#                                                                          self.multi2[0]["test_C12"],
#                                                                          self.multi2[0]["test_C13"]
#                                                                          ])