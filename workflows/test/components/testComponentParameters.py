#
# Copyright (C) 2015 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
from subprocess import Popen, PIPE

from jflow.component import Component

class TestComponentParameters (Component):
    def define_parameters(self, test_B1, test_B2, test_B3, test_B4, test_B5, test_B6, test_B7, test_B8, test_B9, test_B10, test_B11, test_B12, test_B13, test_C1, test_C2, test_C3, test_C4, test_C5, test_C6, test_C7, test_C8, test_C9, test_C10, test_C11, test_C12, test_C13):
        self.add_parameter("test_B1", "float null.", type="float", default=test_B1)
        self.add_parameter("test_B2", "float default 1.", type="float", default=test_B2)
        self.add_parameter("test_B3", "int default 2.", type=int, default=test_B3)
        self.add_parameter("test_B4", "int null.", type=int, default=test_B4)
        self.add_parameter("test_B5", "date default str 20/08/2014.", type="date", default=test_B5)
        self.add_parameter("test_B6", "date default null.", type="date", default=test_B6)
        self.add_parameter("test_B7", "string default str None.", default=test_B7)
        self.add_parameter("test_B8", "string null.", default=test_B8)
        self.add_parameter("test_B9", "bool default True.", type="bool", default=test_B9)
        self.add_parameter("test_B10", "bool default false.", type="bool", default=test_B10)
        self.add_parameter("test_B11", "bool default null.", type="bool", default=test_B11)
        self.add_input_file("test_B12", "Multiple InputFile default null.", default=test_B12)
        self.add_input_file("test_B13", "Multiple InputFile default application.properties.", default=test_B13)
        self.add_parameter("test_C1", "Multiple float null.", type="float", default=test_C1)
        self.add_parameter("test_C2", "Multiple float default 1.", type="float", default=test_C2)
        self.add_parameter("test_C3", "Multiple int default 2.", type=int, default=test_C3)
        self.add_parameter("test_C4", "Multiple int null.", type=int, default=test_C4)
        self.add_parameter("test_C5", "Multiple date default str '20/08/2014'.", type="date", default=test_C5)
        self.add_parameter("test_C6", "date default null.", type="date", default=test_C6)
        self.add_parameter("test_C7", "Multiple string default str 'None'.", default=test_C7)
        self.add_parameter("test_C8", "Multiple string null.", default=test_C8)
        self.add_parameter("test_C9", "Multiple bool default True.", type="bool", default=test_C9)
        self.add_parameter("test_C10", "Multiple bool default 3.", type="bool", default=test_C10)
        self.add_parameter("test_C11", "Multiple bool default null.", type="bool", default=test_C11)
        self.add_input_file("test_C12", "Multiple InputFile default null.", default=test_C12)
        self.add_input_file("test_C13", "Multiple InputFile default application.properties.", default=test_C13)

    def process(self):
        print ("###################### Component ############################")
        for attr in self.params_order:
            value = getattr(self, attr)
            print (attr)
            print (value)
            print(("class   : " + value.__class__.__name__))
#             print "is None : " + str(value is None)
            print(("== None : " + str(value == None)))
            print ("")

    def get_version(self):
        return "-"
