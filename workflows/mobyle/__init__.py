#
# Copyright (C) 2015 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from jflow.workflow import Workflow

class Mobyle (Workflow):

    def get_description(self):
        return "Trim, degap, cut and perform some statistics on a sequence in fasta format."

    def define_parameters(self, function="process"):
        self.add_input_file("fasta_file", "The input fasta file", required=True, file_format="fasta")

    def process(self):
        # Filtering with mobyle components
        trimseq = self.add_component("Trimseq",[], {'e_sequence' : self.fasta_file })
        degapseq = self.add_component("Degapseq",[], {'e_sequence' : trimseq.e_outseq })
        cutseq  = self.add_component("Cutseq",[], {'e_sequence' : degapseq.e_outseq , 'e_from' : 1,  'e_to' : 15 })
        wordcount = self.add_component('Wordcount', [], { 'e_wordsize' : 3, 'e_sequence' : cutseq.e_outseq })
