#
# Copyright (C) 2015 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from jflow.workflow import Workflow


class QuickStart (Workflow):
    
    def get_description(self):
        return "Align reads against a reference genome"

    def define_parameters(self, function="process"):
        self.add_input_file_list("reads", "Which read files should be used", file_format="fastq", required=True)
        self.add_input_file("reference_genome", "Which genome should the read being align on", file_format="fasta", required=True)

    def process(self):
        # index the reference genome
        bwaindex = self.add_component("BWAIndex", [self.reference_genome])
        # align reads against the indexed genome
        bwamem = self.add_component("BWAmem", [bwaindex.databank, self.reads])
         