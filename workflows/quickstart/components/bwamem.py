#
# Copyright (C) 2015 INRA
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from jflow.component import Component
from jflow.abstraction import MultiMap

from weaver.function import ShellFunction


class BWAmem (Component):

    def define_parameters(self, reference_genome, reads):
        self.add_input_file_list( "reads", "Which reads files should be used.", default=reads, required=True )
        self.add_input_file("reference_genome", "Which reference file should be used", default=reference_genome, required=True)
        self.add_output_file_list("sam_files", "The BWA outputed file", pattern='{basename_woext}.sam', items=self.reads)
        self.add_output_file_list("stderr", "The BWA stderr file", pattern='{basename_woext}.stderr', items=self.reads)

    def process(self):
        bwamem = ShellFunction(self.get_exec_path("bwa") + " mem " + self.reference_genome + " $1 > $2 2>> $3", cmd_format='{EXE} {IN} {OUT}')
        bwamem = MultiMap(bwamem, inputs=[self.reads], outputs=[self.sam_files, self.stderr], includes=[self.reference_genome])
        